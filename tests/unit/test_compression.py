import os

import pytest

import linflow.compression

from .test_genome import SAMPLE_GENOMES

# SAMPLE_GENOMES = ["tests/data/NC_003197.2.fasta", "tests/data/NC_003197.2.fasta.gz",
#    "tests/data/NC_003197.2.fasta.tar", "tests/data/NC_003197.2.fasta.tar.gz"]


@pytest.mark.parametrize("file1", SAMPLE_GENOMES)
@pytest.mark.parametrize("file2", SAMPLE_GENOMES)
def test_hashing(file1, file2):
    assert linflow.compression.hash_genome(file1) == linflow.compression.hash_genome(
        file2
    )


@pytest.mark.parametrize("sample_genome", SAMPLE_GENOMES)
def test_uniform_processing(sample_genome):

    project_root_dir = os.getcwd()
    base_path = os.path.dirname(sample_genome)
    base_tmp_path = os.path.join(base_path, "tmp")
    os.makedirs(base_tmp_path, exist_ok=True)
    uniform_file = os.path.join(
        base_tmp_path, f"{os.path.basename(sample_genome)}.fasta.gz"
    )
    fasta_tmp, extracted_flag = linflow.compression.add_uniform_input_genome(
        base_tmp_path, sample_genome, uniform_file
    )
    fasta_tmp2 = f"{fasta_tmp}.fasta"
    if sample_genome != fasta_tmp:
        os.makedirs(os.path.dirname(fasta_tmp2), exist_ok=True)
        os.rename(fasta_tmp, fasta_tmp2)
        fasta_tmp = fasta_tmp2
    assert linflow.compression.hash_genome(
        sample_genome
    ) == linflow.compression.hash_genome(uniform_file)
    if os.path.isfile(fasta_tmp):
        assert linflow.compression.hash_genome(
            fasta_tmp
        ) == linflow.compression.hash_genome(uniform_file)
    os.remove(uniform_file)
