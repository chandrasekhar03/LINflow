import os
import re

import linflow.exceptions
import linflow.lindb
import linflow.lindb_connect
import linflow.util

MODULE_PATH = os.path.dirname(linflow.lindb.__file__)


def test_db_authentication():
    conn, cursor = linflow.lindb_connect.connect_to_db()
    assert conn is not None
    assert cursor is not None


def test_db_creation():
    workspace = linflow.util.get_pytest_workspace()
    linflow.lindb_connect.drop_database(workspace)
    db = linflow.lindb.LINdb(workspace, create=True)
    db.remove()
    db.initiate()
    result = db.execute(
        f"SELECT count(*) FROM INFORMATION_SCHEMA.TABLES "
        f"WHERE TABLE_SCHEMA = '{workspace}';"
    )
    text = ""
    with open(os.path.join(MODULE_PATH, "lindb.sql"), "r") as sqlfile:
        text = " ".join(sqlfile.readlines())
    matches = re.findall(r"CREATE", text)
    db.remove()
    assert int(result[0][0]) > len(matches)


def test_db_selection():
    try:
        workspace = linflow.util.get_pytest_workspace()
        linflow.lindb_connect.drop_database(workspace)
        db = linflow.lindb.LINdb(workspace, create=True)
        db.remove()
        db.initiate()
        db.execute("SELECT * from Genome")
        db.remove()
    except Exception as e:
        assert e is None
