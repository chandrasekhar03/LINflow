import os

import pytest

import linflow.linflow as lf
import linflow.sourmash_custom4 as sm4
import linflow.util

DEFAULT_FASTA = "tests/data/fasta/NC_003197.2.fasta"
DEFAULT_SIG = "tests/data/sig/NC_003197.2.sig"


@pytest.mark.parametrize("k_mer", [21, 51])
@pytest.mark.parametrize(
    "num_sketch, scaled_sketch",
    [
        (0, 1000),
        (0, 2000),
        (0, 3000),
        (1000, 0),
        (2000, 0),
        (3000, 0),
    ],
)
def test_all_sourmash(k_mer, num_sketch, scaled_sketch):
    workspace = linflow.util.get_pytest_workspace(
        extra=[k_mer, num_sketch, scaled_sketch]
    )
    fasta_path = os.path.realpath(DEFAULT_FASTA)
    sig_path = linflow.util.get_filepath(workspace, fasta_path, filetype="sig")

    adapter = sm4.SourmashSigFilter()
    sigs = adapter.create_sketch(
        fasta_path,
        sig_path,
        kmer_size=sm4.DEFAULT_KMERS,
        num_sketch=num_sketch,
        scaled_sketch=scaled_sketch,
    )
    result_df = adapter.compare_sketch(
        sigs[sm4.DEFAULT_KMERS.index(k_mer)], [sig_path], k_mer=k_mer
    )
    numeric_results = result_df.iloc[0].to_dict()
    for key in ["signature", "potential_false_negative", "ignore_abundance"]:
        numeric_results.pop(key, None)

    for key, val in numeric_results.items():
        assert val == 1
    # no false negative
    try:
        assert not result_df.iat[
            0, result_df.columns.get_loc("potential_false_negative")
        ]
    except Exception:
        assert not result_df.iat[0, result_df.columns.get_loc("ignore_abundance")]

    lf.remove(workspace=workspace)
