#!/bin/bash

#Usage
#bash ~/load_file.sh file.csv database.table warning_file "columns,column2,..." 'delimiter' "date_column1,date_col2,..." &

file=$(realpath $1)
table=$2
warnings=$(realpath $3)
columns=$((0+$(head -1 $file | awk -F'$' '{print NF}')))
list="$4"
delimiter=$5
#list="col1"
dates=$6

D="set "
for i in $(echo $dates | sed "s/,/ /g")
do
    	D="$D$i = STR_TO_DATE(@$i,'%d-%b-%Y %H:%i:%s'),"
	list=$(echo $list | sed "s/,$i,/,@$i,/g")
	list=$(echo $list | sed "s/^$i,/@$i,/g")
	list=$(echo $list | sed "s/,$i$/,@$i/g")
done
D=$(echo $D | rev | cut -f 2- -d ',' | rev)

if [ -z $6 ]; then D="";fi
#for i in `seq 2 $columns`;do list="$list,col$i";done

#echo $list
#echo $D
mysql --local-infile=1 --execute="LOAD DATA LOCAL INFILE '$file' IGNORE INTO TABLE $table FIELDS TERMINATED BY '"$delimiter"' OPTIONALLY ENCLOSED BY '\"' IGNORE 1 LINES ($list) $D; SHOW WARNINGS" > $warnings
