#!/usr/bin/env python3
import argparse
import json
import os
import sys
from typing import Union

import numpy as np

import linflow.util

# adds linflow to the path when run as an uninstalled module (source code)
sys.path.append(os.path.join(os.path.dirname(__file__), "src"))

PRIMITIVE = (str, int, float, bool, None)


def main(argv: list):  # pylint: disable=redefined-outer-name
    """Starting point of running the LINflow module

    Args:
            argv (list, optional): Arguments passed to the LINflow argument parser. Defaults to [].
    """
    # os.chdir(os.path.dirname(os.path.abspath(__file__)))
    import linflow.__main__ as module_main

    add_genomerxiv_arg_parser()
    run_result = module_main.main(argv, console_logging=False)
    # print(json.dumps(run_result, default=lambda o: "<not serializable>"))
    print(json.dumps(todict(run_result), default=lambda o: "null"))
    # print(run_result)


def todict(obj, classkey=None) -> Union[dict, list, int, float, str]:
    if isinstance(obj, dict):
        data = {}
        for k, v in obj.items():
            if hasattr(k, "dtype"):
                data[k.item()] = todict(v, classkey)
            else:
                data[k] = todict(v, classkey)
        return data
    if hasattr(obj, "_ast"):
        return todict(obj._ast())
    if hasattr(obj, "__iter__") and not isinstance(obj, str):
        return [todict(v, classkey) for v in obj]
    if hasattr(obj, "__dict__"):
        data = dict(
            [
                (key, todict(value, classkey))
                for key, value in obj.__dict__.items()
                if not callable(value) and not key.startswith("_")
            ]
        )
        if classkey is not None and hasattr(obj, "__class__"):
            data[classkey] = obj.__class__.__name__
        return data
    # if numpy type convert to python native type
    if hasattr(obj, "dtype"):
        return obj.item()
    # else
    return obj


def add_genomerxiv_arg_parser():
    from linflow.arg_parser import main_parser, router_parser, subparser

    scheme_parser = argparse.ArgumentParser(add_help=False)
    scheme_parser.add_argument(
        "-s",
        "--scheme",
        dest="SchemeID",
        type=int,
        default="1",
        help="the schemeID LINs are based. Default: 1",
    )

    lingroup_parser = argparse.ArgumentParser(add_help=False)

    lingroup_parser.add_argument(
        "--clean",
        dest="clean",
        nargs="?",
        const=True,
        default=False,
        help="Remove ",
    )

    tax_type_parser = argparse.ArgumentParser(add_help=False)

    tax_type_parser.add_argument(
        "-tt",
        "--tax-type",
        dest="TaxTypeID",
        type=int,
        default=1,
        help="Type of taxonomy to add the LINgroups to e.g. {NCBI:1 GTDB:2 genomeRxiv:3 custom:4}. This overwrites taxtypeID in file (if applicable).",
    )

    add_prefix_parser = argparse.ArgumentParser(add_help=False)

    add_prefix_parser.add_argument(
        "-p",
        "--prefixes",
        dest="New_prefix",
        required=True,
        type=linflow.util.valid_dir,
        help="CSV file containing the Prefixes. required columns: [rank,taxTypeID,title,prefix(string)] Optional [lingroupID,desc,link]",
    )
    subparser.add_parser(
        "add_prefix",
        parents=[
            main_parser,
            add_prefix_parser,
            tax_type_parser,
            lingroup_parser,
            scheme_parser,
        ],
        help="adds prefixes to existing or new LINgroups",
    )

    add_radii_parser = argparse.ArgumentParser(add_help=False)
    add_radii_parser.add_argument(
        "-r",
        "--radii",
        dest="Radii_file",
        type=linflow.util.valid_file,
        required=True,
        help="TSV file containing the genome assemblyIDs and cluster radii. required columns: [genome,rankID,taxonomy,radius] Optional [lingroupID,desc,link,taxtypeID]",
    )

    add_radii_parser.add_argument(
        "--tax-type",
        dest="TaxTypeID",
        type=int,
        default=1,
        help="Type of taxonomy to add the LINgroups to e.g. {NCBI:1 GTDB:2 gRxiv:3 custom:4}. This overwrites taxtypeID in file.",
    )

    subparser.add_parser(
        "lingroup_radii",
        parents=[main_parser, add_radii_parser, lingroup_parser, scheme_parser],
        help="adds LINgroups based on genome LINs and radii of influence",
    )

    prefix_members = argparse.ArgumentParser(add_help=False)
    lingroupID_or_prefix_list = prefix_members.add_mutually_exclusive_group(
        required=True
    )

    lingroupID_or_prefix_list.add_argument(
        "-ps",
        "--prefixes",
        dest="PrefixList",
        type=str,
        help="list of prefixes to find genomesIDs. e.g of two prefixes (1,2;1,3,0;)",
    )
    lingroupID_or_prefix_list.add_argument(
        "-lg",
        "--lingroup",
        dest="LINgroupID",
        type=int,
        help="the lingroupID to find member genomeIDs",
    )
    subparser.add_parser(
        "lingroup_members",
        parents=[main_parser, prefix_members, scheme_parser],
        help="Find all genomes which are members of a LINgroup or prefix",
    )

    add_attributes = argparse.ArgumentParser(add_help=False)
    add_attributes.add_argument(
        "-f",
        "--attribute-file",
        dest="File",
        required=True,
        type=linflow.util.valid_file,
        help="CSV file containing the genomeID aws one column and other column headers as attributeIDs with data",
    )

    add_attributes.add_argument(
        "-d",
        dest="Date_cols",
        nargs="+",
        type=int,
        help="Date columns to parse as a date object",
    )

    add_attributes.add_argument(
        "-u",
        dest="Update_cols",
        nargs="+",
        type=int,
        help="Attribute columns to force overwrite in the database if they exists",
    )

    subparser.add_parser(
        "add_attributes",
        parents=[main_parser, add_attributes],
        help="Add or update genome attributes",
    )

    find_lingroup = argparse.ArgumentParser(add_help=False)

    # keyword_rank_exgroup = find_lingroup.add_mutually_exclusive_group()

    find_lingroup.add_argument(
        "-k",
        "--keyword",
        dest="Keyword",
        type=str,
        help="a keyword existing in any taxonomic level of the LINgroup",
    )

    find_lingroup.add_argument(
        "-tr",
        "--tax-ranks",
        dest="TaxRanks",
        type=str,
        default="",
        help="""JSON of ranks to search (intersection of multiple) e.g. {rankID:rankName, 6: "pantoea", 7: "pantoea rwandensis_b"}""",
    )

    find_lingroup.add_argument(
        "-b",
        "--submitter",
        dest="Submitter",
        type=str,
        help="submitter of the LINgroup",
    )

    subparser.add_parser(
        "find_lingroup",
        parents=[
            main_parser,
            find_lingroup,
            tax_type_parser,
        ],
        help="finds LINgroups based on taxonomic ranks",
    )


if __name__ == "__main__":
    main(sys.argv)
