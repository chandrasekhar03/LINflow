import json
import os
from typing import Union, overload

import numpy as np
import pandas as pd

import linflow.lin
import linflow.lindb
import linflow.logger
import linflow.scheme
import linflow.tax_enums
import linflow.taxonomy
import linflow.util

logger = linflow.logger.get_logger(__name__)


class LINgroup:
    id: int = 0
    title: str = ""
    note: str = ""
    schemeID: int = 0
    taxType: str = ""
    members: list[dict[int, int]] = []

    def _set_fields(self, fields: list[str], source: dict, required=False):
        for field in fields:
            if required and field not in source:
                raise AttributeError("Missing Attribute %s from %s", field, source)
            if field in source and source[field] is not None:
                self.__setattr__(field, source.get(field))

    @overload
    def __init__(
        self,
        db: linflow.lindb.LINdb,
        *,
        title: str,
        description: str,
        members: list[linflow.lin.LIN],
        schemeID: int = 1,
        taxType="ncbi",
        register=False,
    ): ...

    @overload
    def __init__(self, db: linflow.lindb.LINdb, *, lingroupID: int): ...

    def __init__(self, db: linflow.lindb.LINdb, *, register=False, **kwargs):
        self.db = db
        if (lingroupID := kwargs.get("lingroupID")) is not None:
            df = db.get_lingroup(lingroupID)
            if df.empty:
                raise NameError("LINgroup with ID %s not registered", self.id)
            self._set_fields(
                ["title", "note", "members"], df.iloc[0].to_dict(), required=True
            )
            self.id = lingroupID
            self.schemeID = df.iat[0, df.columns.get_loc("schemeSetID")]

        else:
            self._set_fields(
                ["lingroupID", "schemeID", "title", "note", "members"],
                kwargs,
                required=True,
            )

        self.tax_type = kwargs.get("taxType", "ncbi")

        if register:
            self.register()

    @property
    def tax_type(self):
        return self._tax_type

    @tax_type.setter
    def tax_type(self, tax_type: Union[int, str]):
        if isinstance(tax_type, int):
            self._tax_type = linflow.tax_enums.TaxType(tax_type).name
        else:
            self._tax_type = linflow.tax_enums.TaxType[tax_type].name

    def get_members(self):
        members_df = self.db.get_lingroup_prefixes(self.id)
        self.members = [json.loads(m) for m in members_df["member"]]
        return self.members

    def register(self, update=False):
        self.id = self.db.add_lingroup(
            self.schemeID,
            self.title,
            desc=self.note,
            taxTypeID=linflow.tax_enums.TaxType[self.taxType].value,
        )
        if self.members is not None:
            self.db.add_prefix_to_lingroup(
                self.id, self.schemeID, self.members, clean=(not update)
            )

    def remove(self, raise_exception=True):
        if not self.db.get_lingroup(self.id).empty:
            self.db.remove_lingroups([self.id])
        elif raise_exception:
            raise NameError("LINgroup with ID %s not registered", self.id)

    def is_member(self, lin: linflow.lin.LIN) -> bool:
        if not lin.dlin:
            raise AttributeError("Can not check None LIN membership")
        if lin.scheme.id != self.schemeID:
            return False

        for dlin in self.members:
            if linflow.util.dlin_is_subset(dlin, lin.dlin):
                return True
        return False


def define_lingroups(
    db: linflow.lindb.LINdb,
    lingroup_df: pd.DataFrame,
    schemeSetID: int,
    clean=False,
    **kwargs,
):
    """Circumscribes a LINgroup and connects it to its prefixes

    Args:
        db (LINdb): database object to add the LINgroup to
        lingroup_df (pd.DataFrame): required columns ["rankID", "taxTypeID", "prefix"] Optional ["lingroupID", "taxonomy", "title", "desc", "link"]
        schemeSetID (int): ID of the scheme to define the LINgroup
        clean (bool, optional): whether to remove current prefixes form the currently defend lingroups. Defaults to False.

    """
    lingroup_count = 0
    tax_obj = linflow.taxonomy.Taxonomy(db, taxid_dict={}, tax_type="ncbi")
    if "title" in lingroup_df:
        tax_string_cleaning(lingroup_df, "title")
    # TODO: Change the adds here to be done in one query (batch)
    for idx, row in lingroup_df.iterrows():
        dlins = [linflow.util.slin2dlin(dlin, prefix=True) for dlin in row["prefix"]]
        if (
            "lingroupID" not in row
            or pd.isna(row["lingroupID"])
            or not row["lingroupID"]
        ):
            lingroup_df.at[idx, "lingroupID"] = lingroupID = db.add_lingroup(
                schemeSetID, **row.to_dict()
            )
        else:
            # there should only be one LINgroupID here for the same
            # ["rankID", "taxTypeID", "title"] assert fails otherwise
            if isinstance(row["lingroupID"], int):
                lingroup_df.at[idx, "lingroupID"] = lingroupID = row["lingroupID"]
            elif len(row["lingroupID"]) == 1:
                lingroup_df.at[idx, "lingroupID"] = lingroupID = int(
                    row["lingroupID"][0]
                )
            else:
                # TODO: write an exception/log here
                exit(-1)
        new_prefixIDs = db.add_prefix_to_lingroup(
            lingroupID, schemeSetID, members=dlins, clean=clean
        )
        if lingroup_count % 1000 == 0:
            logger.info("LINgroup %d from %d", lingroup_count, len(lingroup_df))
        lingroup_count += 1
    # adding taxonomy
    if "taxonomy" in lingroup_df.columns:
        # parse the taxonomy string
        # change the string ranks to int ranks based on the Taxonomy class
        lingroup_df["taxonomy"] = lingroup_df["taxonomy"].astype(str)

        tax_string_df = (
            pd.DataFrame(lingroup_df["taxonomy"].apply(tax_obj.taxstring2list).tolist())
            .set_index(lingroup_df.index)
            .explode(column=[0, 1])
            .rename(columns={0: "rankID", 1: "taxon"})
        )
        if "title" in lingroup_df.columns:
            title_taxon_df = lingroup_df[["rankID", "title"]].rename(
                {"title": "taxon"}, axis=1
            )
            # duplicate indices between tax_string and title (will only keep title)
            dup_idx = pd.merge(
                tax_string_df.reset_index(),
                title_taxon_df.reset_index(),
                on=["index", "rankID"],
            )["index"]
            tax_string_df = pd.concat(
                [tax_string_df, title_taxon_df[~title_taxon_df.index.isin(dup_idx)]],
                axis=0,
            )

        # find explicit taxonomic columns (e.g. genus, species, clade, ...)
        # and overwrite the tax_string information
        tax_ranks_names = tax_obj.RankName_ID_table()
        named_rank_df = lingroup_df[
            np.intersect1d(lingroup_df.columns, [*tax_ranks_names])
        ]

        # clean taxonomic columns with rank names where cells from s__XXXXX -> XXXXX
        for col in named_rank_df.columns:
            tax_string_cleaning(lingroup_df.astype(str), col)
            # filling explicit (given rank column) col with taxonomy_string data if explicit is null
            tax_string_rows = tax_string_df["rankID"] == tax_ranks_names[col]
            tax_string_df.loc[
                tax_string_rows,
                "taxon",
            ] = lingroup_df[
                col
            ].fillna(tax_string_df.loc[tax_string_rows, "taxon"])

        result_df = lingroup_df.merge(
            tax_string_df,
            how="right",
            right_index=True,
            left_index=True,
        )
        if "rankID_x" in result_df:
            result_df.rename(columns={"rankID_x": "rankID_lingroup"}, inplace=True)
        if "rankID_y" in result_df:
            result_df.rename(columns={"rankID_y": "rankID"}, inplace=True)

        lingroup_tax_add_csv_path = os.path.join(
            linflow.util.workspace_dir(db.db_name, create=False),
            "new_lingroups.csv",
        )
        result_df.to_csv(lingroup_tax_add_csv_path)
        logger.info("LINgroup summary saved at %s", lingroup_tax_add_csv_path)
        result_df = result_df[
            result_df.columns.intersection(
                ["lingroupID", "taxTypeID", "rankID", "parent", "taxon", "taxID"]
            )
        ]
        if "taxTypeID" not in result_df:
            result_df["taxTypeID"] = int(
                kwargs.get("TaxtypeID", linflow.tax_enums.TaxType.gtdb.value)
            )  # defaults to latest GTDB version

        # overwrite with exclusive rank columns
        # and within-species columns
        # set lowest rank as title or overwrite title by column
        # initialize the taxonomy_df
    else:
        # only gets columns that are usable (could be non-existent)
        result_df = lingroup_df[
            lingroup_df.columns.intersection(
                [*("lingroupID", "taxTypeID", "rankID", "title", "parent", "taxID")]
            )
        ]
        if "title" in result_df.columns:
            result_df = result_df.rename({"title": "taxon"}, axis=1)
    db.update_lingroup_taxonomy(result_df.dropna())
    logger.info("Taxonomy added. %d entries", len(result_df))


def tax_string_cleaning(
    df: pd.DataFrame, target_col: str, return_only=False
) -> pd.Series:
    """clean taxonomic column within a dataframe s__XXXXXXXXXXX -> XXXXXXXXXXX

    Args:
        df (pd.DataFrame): object to be cleaned; Column is overwritten (aka inplace) by default
        target_col (str): columns to test and clean
        return_only (bool) decides whether the column is only returned or overwritten as well

    Returns:
        pd.Series: cleaned column
    """
    row_needs_cleaning = df[target_col].str.contains("^[a-zA-Z]__")
    if np.any(row_needs_cleaning) and not return_only:
        df.loc[row_needs_cleaning, target_col] = df.loc[
            row_needs_cleaning, target_col
        ].apply(lambda x: x[3:])
    return df.loc[row_needs_cleaning, target_col]
