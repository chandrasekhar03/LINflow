#!/usr/bin/env python3
import math

# used for asynchronous parallelism
import multiprocessing as mp
import os
from multiprocessing.pool import ThreadPool
from typing import TYPE_CHECKING, Optional, Tuple, Type

import numpy as np
from pandas import DataFrame, Series

import linflow.exceptions
import linflow.lindb
import linflow.lindb_connect
import linflow.logger
import linflow.util

if TYPE_CHECKING:
    from linflow.scheme import SchemeSet

BASE_SCHEME_PRINT_HEADERS = ["Scheme_Set_ID", "Size", "Basic_Blocks", "Description"]
SCHEME_SET_PRINT_HEADERS_SIMPLE = [
    "Basic_Scheme_ID",
    "Min",
    "Max",
    "Size",
    "Description",
]
SCHEME_SET_PRINT_HEADERS_EXTENDED = [
    "Basic_Scheme_ID",
    "Min",
    "Max",
    "Size",
    "Thresholds",
    "Filter_IDs",
    "Description",
]
HALF_CPU_COUNT = math.floor(float(mp.cpu_count()) / 4)
POOL: ThreadPool
logger = linflow.logger.get_logger(__name__)

# circular LIN relationships
# select * from Comparison  join LINTree on refID=genomeID having queryID=parent;

# get genomes what do not have a corresponding parent with a LIN
# select * from LINTree l1 left join LINTree l2 on l1.parent=l2.genomeID having l1.parent > 1 and l2.genomeID is NULL;
# same but invalid neighbor
# select * from LINTree l1 left join LINTree l2 on l1.neighbor=l2.genomeID having l1.genomeID > 2 and l2.genomeID is NULL;

# TODO:  missing when copied
# existing genomes with missing LINs
# `mysql gtdb207 -e "select * from LINTree left join gtdb207_sbt.LINTree as SBT using (genomeID) having SBT.idx is NULL;"`
# existing genomes with LINs but missing comparisons (as query, and ref for others)
# `mysql gtdb207_sbt -e "insert ignore into gtdb207_sbt.Comparison (select gtdb207.Comparison.* from gtdb207.Comparison join (select genomeID from LINTree_back left join Comparison on genomeID=queryID where refID is NULL and genomeID>2) as missing on queryID=genomeID or refID=genomeID);"`


def workspace_empty(workspace, log_error=True):
    """Checks if both the workspace directory and database are non-existent

    Args:
            workspace (str): name of the workspace
            log_error (bool, optional): whether to log the errors or not. Defaults to True.

    Returns:
            bool: True if both directory and database do not exist
    """
    from linflow.exceptions import UncleanWorkspaceError

    workspace_path = linflow.util.workspace_dir(workspace)
    if os.path.isdir(workspace_path) and linflow.lindb_connect.database_exists(
        workspace
    ):
        if log_error:
            logger.exception(UncleanWorkspaceError(workspace))
            logger.error(
                "Provided workspace is not empty, please use a clean environment"
            )
        return False
    return True


def remove(**kwargs):
    """
    Deletes the databases with tables/indexes/relationships and workspace folders

    Parameters (included in kwargs)
    ----------
    workspace : str
            name of the workspace/table to be created
    """
    from linflow.lindb_connect import connect_to_db

    workspace = kwargs.get("workspace", "")
    logger.info("Removing workspace %s", workspace)
    project_folder = os.path.realpath(linflow.util.workspace_dir(workspace))
    if os.path.isdir(project_folder):
        linflow.util.rmtree2(project_folder)
    try:
        conn, c = connect_to_db(workspace, log_error=False)
        c.execute(f"DROP DATABASE IF EXISTS {workspace}")
        # c.execute(f"DROP TABLESPACE {workspace}")
        logger.info("Done")
    except Exception as e:
        logger.debug(e)


def init(**kwargs) -> Optional[linflow.lindb.LINdb]:
    """
    initialize the databases with tables/indexes/relationships and folder hierarchy

    Args:
    workspace : str
            name of the workspace/table to be created
    """

    from linflow.exceptions import UncleanWorkspaceError
    from linflow.scheme_init import initialize_schemes
    from linflow.util import workspace_dir

    db: linflow.lindb.LINdb
    try:
        project_root_dir = os.getcwd()
        os.makedirs(os.path.join(project_root_dir, "workspaces"), exist_ok=True)

        workspace = kwargs.get("workspace", "")
        workspace_path = workspace_dir(workspace)
        if not workspace_empty(workspace):
            linflow.exceptions.handle_exception(
                UncleanWorkspaceError(workspace), logger
            )
        else:
            logger.debug("Creating and moving to dir %s", workspace)
            os.makedirs(workspace_path, exist_ok=True)
            # os.chdir(workspace_path)
            db = linflow.lindb.LINdb(workspace)
            db.initiate()

            initialize_schemes(db, **kwargs)

            logger.debug("Creating sub-directories")
            for groups in linflow.util.SUB_DIRECTORIES.values():
                for name in groups.keys():
                    workspace_dir(workspace, name, create=True)
            # os.chdir(project_root_dir)
            logger.info("Workspace initiation complete")
    except Exception as e:
        remove(**kwargs)
        logger.error("Workspace initialization failed")
        logger.exception(e)
        logger.info("Cleaning up workspace")
        if db is not None:
            db.reset_counter()
        raise e
    return db


def ani_rank_value(measures: list, thresholds) -> float:
    threshold_length = 0 if not thresholds else len(thresholds)
    for i, measure in enumerate(measures):
        if i < threshold_length and thresholds[i] <= measure:
            pass
        elif i < threshold_length and thresholds[i] > measure:
            return 0.0
        else:
            pass
    return measures[0]


def _show_scheme_set(
    db: linflow.lindb.LINdb,
    ids: Optional[list] = None,
    extended=False,
    save: Optional[str] = None,
):
    """displays a number of schemeSets in a table format in two configurations

    Args:
            db ([linflow.lindb.LINdb]): [database/workspace to use]
            ids (Optional[list], optional): [specific schemes to show]. Defaults to None.
            extended (bool, optional): [show full information without table format]. Defaults to False.
            save (Optional[str], optional): [file path to save results]. Defaults to None.
    """
    try:
        schemes = db.get_scheme_sets(ids)
        outfile = open(save, "w") if save is not None else None
    except BaseException as e:
        logger.error(e)
        return DataFrame()
    if not extended:
        print("\t".join(BASE_SCHEME_PRINT_HEADERS) + "\n", file=outfile)
        for _, scheme in schemes.iterrows():
            print(
                f"{scheme['id']:<15}\t"
                f"{scheme['size']:<5}\t"
                f"{np.array2string(np.array(scheme['blocks'])):<20}\t"
                f"{scheme['note']:<50}\n",
                file=outfile,
            )
    else:
        print("\t".join(BASE_SCHEME_PRINT_HEADERS) + "\n", file=outfile)
        for _, scheme in schemes.iterrows():
            print(
                f"{scheme['id']:<15}\t"
                f"{scheme['size']:<5}\t"
                f"{np.array2string(np.array(scheme['blocks']))}\t"
                f"{scheme['note']}\n",
                file=outfile,
            )
    if outfile is not None:
        outfile.close()
        print("Done")
    return schemes


def _show_base_scheme(
    db: linflow.lindb.LINdb,
    ids: Optional[list] = None,
    extended=False,
    save: Optional[str] = None,
):
    """displays a number of baseSchemes in a table format in two configurations

    Args:
            db ([linflow.lindb.LINdb]): [database/workspace to use]
            ids (Optional[list], optional): [specific schemes to show]. Defaults to None.
            extended (bool, optional): [show full information without table format]. Defaults to False.
            save (Optional[str], optional): [file path to save results]. Defaults to None.
    """
    try:
        schemes = db.get_baseScheme(ids)
        outfile = open(save, "w") if save is not None else None
    except Exception as e:
        logger.error(e)
        return DataFrame()

    if not extended:
        print("\t".join(SCHEME_SET_PRINT_HEADERS_SIMPLE) + "\n", file=outfile)
        for _, scheme in schemes.iterrows():
            print(
                f"{scheme['id']:<15}\t"
                f"{scheme['min']:.5f}\t"
                f"{scheme['max']:.5f}\t"
                f"{scheme['size']:<8}\n",
                file=outfile,
            )
    else:
        print("\t".join(SCHEME_SET_PRINT_HEADERS_EXTENDED) + "\n", file=outfile)
        for _, scheme in schemes.iterrows():
            print(
                f"{scheme['id']:<15}\t"
                f"{scheme['min']:.5f}\t"
                f"{scheme['max']:.5f}\t"
                f"{scheme['size']:<8}\t"
                f"{scheme['thresholds']}\t"
                f"{np.array2string(np.array(scheme['filters']))}\n"
                f"{scheme['note']}\n",
                file=outfile,
            )
    if outfile is not None:
        outfile.close()
        print("Done")
    return schemes


def show_scheme(**kwargs):
    """shows the registered schemes (SchemeSet or BaseScheme) registered in the workspace

    Args:
            workspace (str): name of the workspace/table to check for schemes
            Output (str, optional): the filename to save summery of available schemes
            SchemeID (list, optional): list of schemeIDs to show. Default: shows all
            Show_Basic (bool, optional): used to show a simple summary of the basic schemes. Default: False
            Extended (bool, optional): used to show full details when displaying the basic schemes.
                    Default: False
    """
    db = linflow.lindb.LINdb(kwargs.get("workspace", ""))
    try:
        if kwargs.get("SchemeID") is None:
            ids = None
        else:
            ids = np.array(kwargs.get("SchemeID"), dtype=np.intc).tolist()
    except Exception as e:
        logger.error(e)
        return

    if kwargs.get("Show_Basic"):
        function_handle = _show_base_scheme
    else:
        function_handle = _show_scheme_set

    return function_handle(
        db, ids=ids, extended=kwargs.get("Extended", False), save=kwargs.get("Output")
    )


def add_simple_scheme(**kwargs):
    """Adds a new simple scheme

    Args:
            workspace (str): name of the workspace/table to add scheme to
            Scheme_Min (float, optional): minimum %% similarity the scheme measures
            Scheme_Max (float, optional): maximum %% similarity the scheme measures
            Scheme_Step (float, optional): the min-max range will be sliced with this distance
            Thresholds (list, optional): List of thresholds, float (value) or int (topk), to switch to next filter
            Positions (list, optional): define scheme with exact position percentages e.g. 90,95,95.1
            Filter_ID (list, optional): list of filterIDs to add to simple scheme- run in order
            Scheme_desc (str, optional): description of the scheme. Default ''
    """
    db = linflow.lindb.LINdb(kwargs.get("workspace", ""))
    scheme_min = kwargs.get("Scheme_Min", 0.0)
    scheme_max = kwargs.get("Scheme_Max", 100.0)
    scheme_step = kwargs.get("Scheme_Step", 1.0)
    scheme_positions = kwargs.get("Positions")
    scheme_desc = kwargs.get("Scheme_desc")
    if not scheme_positions:
        try:
            len(np.arange(scheme_min, scheme_max + scheme_step, scheme_step))
        except Exception as e:
            logger.error(e)

        bschemeID = db.add_base_scheme(
            min=scheme_min,
            max=scheme_max,
            step=scheme_step,
            thresholds=kwargs.get("Thresholds"),
            description=scheme_desc,
        )
    elif not scheme_min or not scheme_max or not scheme_step:
        logger.error('Numbers should be ordered or be in the format "Min,Step,Max"')
        return
    else:
        try:
            scheme_list = np.array(scheme_positions.split(","), dtype=np.floating)
        except Exception as e:
            linflow.exceptions.handle_exception(e, logger)
            return
        # scheme_str = ",".join(scheme_list.astype(str).tolist())
        bschemeID = db.add_base_scheme(
            positions=scheme_list,
            thresholds=kwargs.get("Thresholds"),
            description=scheme_desc,
        )

    try:
        filterIDs = np.array(kwargs.get("Filter_ID"), dtype=np.intc)
    except Exception as e:
        linflow.exceptions.handle_exception(e, logger)
        return
    db.connect_scheme_filter(bschemeID, filterIDs)

    return _show_base_scheme(db, [bschemeID], extended=True)


def add_complex_scheme(**kwargs):
    """Add a SchemeSet to the database

    Args:
            workspace (str): name of the workspace/table to add scheme to
            SchemeID (list, optional): list of simple schemeIDs to add to complex scheme. run in order
            Descend_Levels (list, optional): number of positions to descend for each simple scheme block
            Scoring_Function (str, optional): function used to translate similarity score to LIN. Default: scores2dlin_default
            Scheme_desc (str, optional): description of the scheme. Default ''
    """
    db = linflow.lindb.LINdb(kwargs.get("workspace", ""))
    try:
        basic_scheme_id = np.array(kwargs.get("SchemeID"), dtype=np.uintc)
        if kwargs.get("Descend_Levels"):
            levels = np.array(kwargs.get("Descend_Levels"), dtype=np.uintc)
    except Exception as e:
        logger.exception(e)
        return

    schemeSetID = db.add_scheme_set(
        basic_scheme_id,
        score=kwargs.get("Scoring_Function"),
        levels=levels,
        note=kwargs.get("Scheme_desc"),
    )
    logger.info("Complex scheme added with ID: %s", schemeSetID)
    return _show_scheme_set(db, [schemeSetID], extended=True)


def identify(**kwargs):
    import time

    import pandas as pd

    import linflow.compression
    import linflow.exceptions
    import linflow.genome
    import linflow.lindb
    import linflow.scheme

    genome_file = os.path.realpath(kwargs.get("Identify_Genome", ""))
    time.sleep(10.0)
    for _ in range(12):
        if linflow.compression.test_gzip(genome_file) or genome_file[-3:] != ".gz":
            break
        time.sleep(5.0)

    if not os.path.isfile(genome_file):
        linflow.exceptions.handle_exception(
            FileNotFoundError(
                "Genome file does not exist '%s'. Terminating process", genome_file
            ),
            logger,
        )
    meta_df = DataFrame(
        [(os.path.basename(genome_file), "identify")],
        columns=["genome file", "strain"],
    )
    meta_df.set_index(["genome file"], inplace=True)

    kwargs["meta_df"] = meta_df
    kwargs["Identify"] = True
    kwargs["Input_Dir"] = os.path.dirname(genome_file)
    kwargs["No_qc"] = True
    identify_obj = genome(**kwargs)
    identify_dict = {k: v.to_dict() for k, v in identify_obj.items()}

    schemeIDs = [int(x) for x in kwargs.get("SchemeID", [1])]
    db = linflow.lindb.LINdb(kwargs.get("workspace", "No Workspace"))
    lingroup_dicts = []
    neighbor_dicts = []
    for schemeID in schemeIDs:
        scheme = linflow.scheme.SchemeSet(db, schemeID=schemeID)
        if not scheme.scheme_blocks or not scheme.scheme_blocks[-1].filterIDs:
            # if there are not scheme blocks or no filters in the last block
            return []
        filterID = scheme.scheme_blocks[-1].filterIDs[-1]
        neighbor_distances = db.get_comparison(
            queryIDs=[*identify_dict],
            filterIDs=[filterID],
        ).set_index(["queryID"])
        # identify_dict["type"] = "identify-table"  # type: ignore

        start_time = time.time()
        df_list = []
        for genomeID, genome_obj in identify_obj.items():
            genome_obj: linflow.genome.Genome
            if genomeID in neighbor_distances.index:
                neighbor_df = neighbor_distances.loc[[genomeID]].reset_index()
                max_rankscore_idx = neighbor_df["rankScore"].idxmax()
                neighbor_row = neighbor_df.loc[max_rankscore_idx]
                identify_obj[genomeID].neighbor = neighbor_row["refID"]
                identify_obj[genomeID].rankScore = neighbor_row["rankScore"]
                identify_obj[genomeID].filterID = neighbor_row["filterID"]
                neighbor_dicts += [
                    {
                        "data": (
                            neighbor_row["refID"].tolist()
                            if isinstance(neighbor_row["refID"].tolist(), list)
                            else [neighbor_row["refID"]]
                        ),
                        "type": "genome-table",
                        "title": "Closest Genome",
                        "subtitle": f"{neighbor_row['rankScore']}% ANI to Target",
                        "schemeID": schemeID,
                    }
                ]
            else:
                identify_obj[genomeID].neighbor = -1
                identify_obj[genomeID].rankScore = 0
                identify_obj[genomeID].filterID = 0
                neighbor_dicts += [{}]

            if lin_dict := genome_obj.get_lin(scheme, False):
                dlin = lin_dict[schemeID]
            else:
                continue

            df_list += [db.search_prefix(dlin, schemeID, zeros=True)]

        if not df_list:
            continue
        covered_lingroups = (
            pd.concat(df_list)
            .drop_duplicates(["prefixID"])
            .set_index(["prefixID"])
            .sort_index()
        )

        lingroup_dict = {
            "data": covered_lingroups.index.tolist(),
            "type": "lingroup-table",
            # "title": "LINgroups",
            # "subtitle": f"{len(covered_lingroups)} described LINgroups",
            "time": [time.time() - start_time],
            "schemeID": schemeID,
        }
        lingroup_dicts += [lingroup_dict]
    dict_genome = {k: v.to_dict() for k, v in identify_obj.items()}

    return [
        {"data": dict_genome, "type": "identify-table"},
        *neighbor_dicts,
        *lingroup_dicts,
    ]


def genome(**kwargs) -> dict:
    """Add genomes to the database

    Args:
    workspace (str): name of the workspace/table to add scheme to
    Input_Dir (str): the directory containing genomes (gz,tar,fasta) to be added
    Metadata (str): the metadata file corresponding to the genomes
    SchemeID (list, optional): the scheme(s) based on which LINs are going to be assigned
    No_qc (bool, optional): do not ignore genomes due to failed quality control
    Identify (bool, optional): specifies if the genomes are to be identified instead of added to the database
    """

    import time

    from pandas import read_csv

    import linflow.exceptions
    import linflow.genome
    import linflow.lindb
    import linflow.measure_base
    import linflow.scheme
    import linflow.util
    from linflow.quality_checker import passes_qc
    from linflow.taxonomy import Taxonomy

    global POOL

    thread_count = int(kwargs.get("Threads", 0)) or HALF_CPU_COUNT
    POOL = ThreadPool(thread_count)

    is_identify = kwargs.get("Identify", False)
    return_genomes: dict[int, linflow.genome.Genome] = {}

    # reading metadata file
    meta_df: DataFrame = kwargs.get("meta_df", DataFrame())
    metadata_file = os.path.realpath(kwargs.get("Metadata", "NO_FILE_GIVEN"))
    if not os.path.isfile(metadata_file) and meta_df.empty:
        linflow.exceptions.handle_exception(
            FileNotFoundError(
                "Metadata file does not exist %s. Terminating process", metadata_file
            ),
            logger,
        )

    try:
        # takes meta_df if it is identify, otherwise expects a metadata file
        meta_df = (
            meta_df
            if not meta_df.empty
            else read_csv(metadata_file, sep=",", header=0, index_col="genome file")
        )
        meta_df.columns = [x.lower() for x in meta_df.columns]
    except BaseException as e:
        logger.exception(e)
        return return_genomes

    # setting workspace
    workspace = kwargs.get("workspace", "")
    if workspace_empty(workspace, log_error=False):
        init(**kwargs)
    db = linflow.lindb.LINdb(workspace)
    ignore_qc = bool(kwargs.get("No_qc"))
    # SchemeSetID
    selected_schemeIDs = kwargs.get("SchemeID", [1])

    # adding genomes incrementally
    for genome_idx, genome_file in enumerate(
        meta_df.index
    ):  # pylint: disable=no-member
        genome_obj = None
        add_start_time = time.time()

        try:
            input_filepath = os.path.join(
                kwargs.get("Input_Dir", os.getcwd()), genome_file
            )

            genome_obj = linflow.genome.Genome(db, filepath=input_filepath)
            tmp_fasta, extracted = genome_obj.process_file()
            if not ignore_qc and not passes_qc(tmp_fasta):
                logger.warning(
                    "genome %s failed quality control. skipping genome", input_filepath
                )
                genome_obj.remove()
                continue

            for scheme_idx, scheme_id in enumerate(selected_schemeIDs):
                new_lin = genome_obj.assign_lin(int(scheme_id), is_identify)
                logger_handler = logger.info if is_identify else logger.debug
                logger_handler(
                    "Genome %s was assigned a LIN: '%s' for scheme %s",
                    genome_obj.id,
                    new_lin.dlin,
                    scheme_id,
                )

            if genome_obj.id:
                return_genomes[genome_obj.id] = genome_obj
            if is_identify:
                continue
            # showing if the genome has LINs for multiple taxonomies (including added now and before)
            genome_lin_schemes = genome_obj.db.get_genome_lin_schemes(genome_obj.id)
            logger.info(
                "Genome %s now has %s LINs in schemes %s. %s",
                genome_obj.id,
                len(genome_lin_schemes),
                list(genome_lin_schemes),
                linflow.util.elapsed_time(add_start_time),
            )

            # adding taxonomy
            # TODO: Add new column to example metadata file
            genome_info = meta_df.iloc[
                genome_idx
            ].to_dict()  # pylint: disable=no-member
            # filter taxonomy from all columns
            default_taxa = Taxonomy(
                db,
                **linflow.util.not_none_kwargs(
                    tax_type=genome_info.get("taxonomy type")
                ),
            )
            tax_cols = {
                key: genome_info[val]
                for key, val in default_taxa.RankID_name_table().items()
                if val in genome_info
            }
            taxa = Taxonomy(
                db,
                **linflow.util.not_none_kwargs(
                    taxid_dict=tax_cols, tax_type=genome_info.get("taxonomy type")
                ),
            )

            taxa.update_taxonomy(genome_obj.id)
        except BaseException as exception:
            logger.error("Adding genome failed: %s", genome_file)
            linflow.exceptions.handle_exception(
                exception, logger, raise_exception=False
            )
            logger.info("Cleaning up workspace")
            clean(**kwargs)
            # if genome is not None:
            # 	db.reset_counter()
            # raise e

    # Do cleanups, database commits, etc needed for measurements
    if selected_schemeIDs and not is_identify:
        measures: list[Type[linflow.measure_base.Measure]] = []
        for schemeID in selected_schemeIDs:
            scheme = linflow.scheme.SchemeSet(db, schemeID=schemeID)
            for blocks in scheme.scheme_blocks:
                for filter in blocks.filters:
                    if (
                        filter.measure
                        and issubclass(filter.measure, linflow.measure_base.Measure)
                        and filter.measure not in measures
                    ):
                        measures += [filter.measure]
                        filter.measure.finalize(**filter.default_args)

    clean(**kwargs)

    if POOL is not None:
        POOL.close()
        POOL.join()
        POOL = None  # type: ignore
    return return_genomes


def remove_genome(
    gid: int, workspace: str, lingroup=None, tmp_dir=None, complete=False
):
    db = linflow.lindb.LINdb(workspace)
    if complete:
        db.remove_genome(gid)
        os.remove(linflow.util.get_filepath(workspace, gid, create=False))
        logger.info("Remove successful for Genome %s", gid)
    else:
        # TODO: check if still working
        db.hide_lin([gid])
        logger.info("Hide successful for Genome %s", gid)

    for representative in linflow.util.SUB_DIRECTORIES["genomes"]:
        if representative == "fasta" and not complete:
            continue
        possible_file = linflow.util.get_filepath(
            workspace, gid, linflow.util.SUB_DIRECTORIES["genomes"][representative]
        )
        if os.path.isfile(possible_file):
            os.remove(possible_file)
    logger.info("Workspace folders cleaned")


# TODO Safe delete: hide + recalculate


def summary(**kwargs):
    """get a summary of the available genomes in the workspace

    Args:
            SchemeID (int, optional): the scheme based on which LINs are going to be assigned. Default: 1
            FilterID (int, optional): filter's ID whose measurements to retrieve. Default: last_filter(-1)

    Returns:
            DataFrame: table summary of the available genomes in the workspace
    """
    from linflow.scheme import SchemeSet

    db = linflow.lindb.LINdb(kwargs.get("workspace", ""))
    schemeIDs = kwargs.get("SchemeID", [1])
    lin_df = DataFrame()
    for schemeID in schemeIDs:
        scheme = SchemeSet(db, schemeID=schemeID)
        filterID = kwargs.get("FilterID", -1)
        if filterID == -1:
            filterID = scheme.scheme_blocks[-1].filterIDs[-1]
        ref_type = {}
        filename = f"lin_summary_scheme{schemeID}.csv"

        df = db.get_all_LINs(schemeID, filterID=filterID).dropna(how="all", axis=1)
        if df.empty:
            logger.critical("There were no LINs assigned to SchemeSet %s", schemeID)
            return df

        dlins = linflow.util.sqldf2dlin(df)
        measure_df = df.set_index("genomeID").drop(columns=["lastValue"])
        measure_df.rename({"parent": "lin_parent"}, axis=1, inplace=True)

        result = []
        for gid, dlin in dlins.items():
            string_lin = linflow.util.dlin2slin(dlin, scheme.length)
            result += [(gid, string_lin, *measure_df.loc[gid])]

        lin_df = DataFrame(
            result, columns=["genomeID", "LIN", *list(measure_df.columns)]
        )
        path_df = db.get_genome_path(lin_df["genomeID"].to_numpy())
        lin_df = lin_df.merge(path_df, on="genomeID", how="left")
        if kwargs.get("Extended", False):
            comparison_df = db.get_top_comparisons(schemeID, filterID)
            lin_df = lin_df.merge(comparison_df, on="genomeID", how="left")
            ref_type = {"refID": "Int64", **ref_type}
            validation = []
            for df_idx, row in lin_df.iterrows():
                idx = int(row["idx"])
                row_valid = (
                    (
                        idx >= len(scheme.positions)
                        and row["rankScore"] >= scheme.positions[-1]
                    )
                    or (
                        scheme.positions[idx - 1]
                        <= row["rankScore"]
                        < scheme.positions[idx]
                    )
                    or (row["rankScore"] == row["idx"] == 0)
                    or (row["rankScore"] < scheme.positions[0] and row["idx"] == 0)
                    or (row["genomeID"] == 2)
                )
                validation += [row_valid]
            lin_df["validation"] = validation
        output_path = os.path.join(linflow.util.workspace_dir(db.db_name), filename)
        lin_df.astype({"genomeID": int, **ref_type}).to_csv(output_path, index=False)
        logger.info("Results saved at: %s", output_path)
    return lin_df


def add_matrix_headers(ordered_headers: list, matrix: np.ndarray, scale: float = 1.0):
    if scale != 1:
        matrix *= scale
    return DataFrame(matrix, columns=ordered_headers, index=ordered_headers)


def infer_similarity_matrix(
    lin_df: DataFrame, scheme: "SchemeSet", scale=0.01
) -> Tuple[Series, np.ndarray]:
    """creates an inferred similarity matrix from the given LIN scheme

    Args:
            lin_df (DataFrame): the LIN dataframe (from LINtree) of the given scheme
            scheme (SchemeSet): the SchemeSet on which the LINs were assigned based upon
            header (bool, optional): Whether the matrix should have a header with genomeIDs. Defaults to False.
            scale (float, optional): scale the matrix by multiplying all numbers with the same constant. Defaults to 1.

    Returns:
            Dataframe: genomeID to matrix position mapping
            np.ndarray: the corresponding similarity matrix
    """
    if lin_df.empty:
        logger.critical("There were no LINs assigned to SchemeSet %s", scheme.id)
        return Series([]), np.array([[]], dtype=np.floating)

    lin_df = lin_df.reset_index()
    min_pos = scheme.positions[0] + (scheme.positions[0] - scheme.positions[1])
    positions = [min_pos, *scheme.positions]
    matrix = np.zeros((len(lin_df), len(lin_df)), dtype=float)
    gid2index = lin_df[["genomeID", "index"]].set_index("genomeID")["index"].to_dict()

    grouped_lins = lin_df.groupby(["idx", "parent"]).agg({"index": list})

    cumulative_siblings = []
    idx: int
    parentID: int
    genomes: list
    for (idx, parentID), genomes in grouped_lins.iterrows():  # type: ignore
        parent_siblings = []
        genomes = genomes[0]
        percentage = positions[idx] * scale
        parent_index = gid2index[parentID] if parentID > 1 else 0

        for gid1 in genomes:
            for gid2 in genomes:
                # genomes differentiated by the current LIN position with the same parent
                if gid1 == gid2:
                    matrix[gid1, gid2] = 1
                else:
                    matrix[gid1, gid2] = percentage
            # all parents other than the immediate one
            if parent_index in cumulative_siblings and len(cumulative_siblings) > 1:
                parent_siblings = np.setdiff1d(
                    cumulative_siblings, [parent_index]
                ).tolist()
            if gid1 != parent_index:
                matrix[gid1, parent_index] = matrix[parent_index, gid1] = percentage
            # set distance to parents equal to distance with immediate parent
            matrix[gid1, parent_siblings] = matrix[parent_siblings, gid1] = matrix[
                parent_index, parent_siblings
            ]
        cumulative_siblings += genomes

    # lastValue == lastValue and index = index => matrix = 1
    identical_df = lin_df.groupby(["idx", "lastValue"]).agg({"index": [list, "count"]})
    for (idx, parent), gid_list in identical_df[identical_df["index"]["count"] > 1][
        "index"
    ]["list"].iteritems():
        for gid1 in gid_list:
            for gid2 in gid_list:
                matrix[gid1, gid2] = 1

    # return lin_df[["index", "genomeID"]], matrix
    return lin_df["genomeID"], matrix


def tree(**kwargs):
    """creates phylogenetic trees based on the LINs

    Args:
    workspace (str): workspace to use LINs from
    SchemeID (int, optional): the scheme based on which LINs are going to be assigned. Default: 1
    Rank_depth (str, optional): genome labels to include in the tree. Number of rank levels from the lowest rank. All=Default(1-20) Some(19,20)
    Tax_type (str, optional): type of taxonomy to include in tree. Default: ncbi
    Matrix_only (bool, optional): only creates a matrix file and skips the tree building process
    """
    from pandas import concat

    from linflow.scheme import SchemeSet

    workspace: str = kwargs.get("workspace", "")
    db = linflow.lindb.LINdb(workspace)
    first_scheme = kwargs.get("SchemeID", [1])[0]
    scheme = SchemeSet(db, schemeID=first_scheme)
    scaling_factor = float(kwargs.get("Scale", 1.0))

    lin_df = db.get_all_LINs(scheme.id)
    mapping_df, similarity_matrix = infer_similarity_matrix(
        lin_df, scheme, scale=scaling_factor
    )

    lower_triangle_matrix = np.tril(similarity_matrix + (-1)).tolist()
    for i, row in enumerate(lower_triangle_matrix):
        for j, cell in enumerate(lower_triangle_matrix[i]):
            if j == i:
                lower_triangle_matrix[i] = row[0 : j + 1]
                break

    rank_depth = kwargs.get("Rank_depth", "1-20")
    ranks2include = linflow.util.parse_range_sequence(rank_depth)
    tax_df = db.get_taxonomy(
        mapping_df.to_list(),
        tax_type=kwargs.get("Tax_type", "ncbi"),
        ranks=ranks2include,
    )
    taxonomy = tax_df.groupby("genomeID").agg({"taxon": "__".join})
    annotation = {}
    for genomeID, dlin in linflow.util.sqldf2dlin(lin_df).items():
        # fixes case where the genome does not have a taxon for the selected taxType
        if genomeID not in taxonomy.index:
            taxonomy = concat(
                [
                    taxonomy,
                    DataFrame({"taxon": [f"Genome-{genomeID}"]}, index=[genomeID]),
                ]
            )
        annotation[taxonomy.at[genomeID, "taxon"]] = np.array2string(
            linflow.util.dlin2lin(dlin, scheme_length=scheme.length), separator=","
        )

    # save matrix with headers
    matrix_df = add_matrix_headers(list(taxonomy["taxon"]), similarity_matrix)
    matrix_df.to_csv(
        os.path.join(
            linflow.util.workspace_dir(workspace), f"matrix_scheme{scheme.id}.tsv"
        ),
        sep="\t",
    )

    if kwargs.get("Matrix_only"):
        return
    from Bio import Phylo
    from Bio.Phylo.TreeConstruction import DistanceMatrix, DistanceTreeConstructor

    from linflow.lin_tree import draw_tree, rename_clades

    constructor = DistanceTreeConstructor()
    trees_dir = linflow.util.workspace_dir(
        workspace, "trees", create=True, change_directory=True
    )

    distance_matrix = DistanceMatrix(list(taxonomy["taxon"]), lower_triangle_matrix)
    nj_tree = constructor.nj(distance_matrix)
    upgma_tree = constructor.upgma(distance_matrix)
    rename_clades(nj_tree.clade)
    rename_clades(upgma_tree.clade)

    # Save phylogenetic trees in newick and pdf format
    Phylo.write(nj_tree, os.path.join(trees_dir, "nj.nw"), "newick")
    Phylo.write(upgma_tree, os.path.join(trees_dir, "upgma.nw"), "newick")
    draw_tree("nj.nw", "nj.pdf", annotation, trees_dir)
    draw_tree("upgma.nw", "upgma.pdf", annotation, trees_dir)

    nj_tree.ladderize()
    upgma_tree.ladderize()

    Phylo.write(nj_tree, os.path.join(trees_dir, "nj_ladderized.nw"), "newick")
    Phylo.write(upgma_tree, os.path.join(trees_dir, "upgma_ladderized.nw"), "newick")
    draw_tree("nj_ladderized.nw", "nj_ladderized.pdf", annotation, trees_dir)
    draw_tree("upgma_ladderized.nw", "upgma_ladderized.pdf", annotation, trees_dir)
    return trees_dir


def clean(**kwargs):
    """cleans the workspace of unnecessary files
    Args:
            workspace(str): name of the workspace to work on
    """
    import time
    from glob import glob

    workspace = kwargs.get("workspace", "NO_WORKSPACE")
    workspace_dir = linflow.util.workspace_dir(workspace)
    if not os.path.isdir(workspace_dir):
        logger.critical("Workspace '%s' does not exist. Exiting", workspace_dir)
        return
    db = linflow.lindb.LINdb(workspace)
    # resets genomeID counter to the minimum unused ID possible
    db.reset_counter()
    # cleans temporary uuid4 folders within the workspace
    linflow.util.clean_uuid_dirs(workspace_dir)
    max_genomeID = int(db.execute("SELECT MAX(genomeID) FROM Genome;")[0][0])
    if max_genomeID < 2:
        logger.warning("No registered genomes. Removing workspace")
        remove(**kwargs)
        return

    # removing all screed files
    screed_files = glob(os.path.join(workspace_dir, "*/*/*_screed"))
    for file in screed_files:
        os.remove(file)

    for type_name, file_type in zip(["tmp"], [".tmp"]):
        type_dir = linflow.util.workspace_dir(workspace, type_name)
        tmp_files = glob(os.path.join(type_dir, "*", f"*{file_type}"))
        type_length = len(file_type)
        logger.debug("Cleaning up %d '%s' files", len(tmp_files), type_name)
        # removing tmp files not having a Genome database entry
        for file in tmp_files:
            try:
                # if the file exists AND (is either more than 1 day old OR
                # largest than the max registered genomeID)
                if os.path.isfile(file) and (
                    time.time() - os.path.getmtime(file) > 86400
                    or int(os.path.basename(file)[: (-1 * type_length)]) > max_genomeID
                ):
                    os.remove(file)
            except BaseException as e:
                linflow.exceptions.handle_exception(e, logger, raise_exception=False)
                if os.path.isfile(file):
                    os.remove(file)


def add_prefix(**kwargs):
    import pandas as pd

    import linflow.exceptions
    import linflow.lindb
    import linflow.lingroup

    workspace = kwargs.get("workspace", "NO_WORKSPACE")
    db = linflow.lindb.LINdb(workspace)

    input_file = kwargs.get("New_prefix", "aa")
    schemeSetID = kwargs.get("SchemeID", 1)
    clean = kwargs.get("clean", False)
    prefix_input_df = pd.read_csv(input_file, header=0)

    # for schemeSetID in schemeSetIDs:
    if not isinstance(schemeSetID, int):
        schemeSetID = int(schemeSetID)

    lingroup_df = (
        prefix_input_df.groupby(["rankID", "taxTypeID", "title", "desc"])
        .aggregate(lambda x: np.unique(x[x.notna()]).tolist())
        .reset_index()
    )
    if "lingroupID" not in lingroup_df.columns:
        lingroup_df["lingroupID"] = np.zeros(len(lingroup_df), dtype=int)

    # combining lingroups by rank (e.g. 1 rank can have multiple prefixes) but only one description is allowed.
    try:
        for col in lingroup_df.columns.difference(set(["rankID", "taxTypeID", "title", "prefix", "lingroupID", "desc"])):  # type: ignore
            lingroup_df[col] = lingroup_df[col].apply(
                lambda x: np.array(x).item() if x else np.nan
            )
    except BaseException as ex:
        logger.error(
            "One LINgroup had multiple non-matching descriptions for the same taxTypeID"
        )
        linflow.exceptions.handle_exception(ex, logger, raise_exception=True)

    linflow.lingroup.define_lingroups(db, lingroup_df, schemeSetID, clean=clean)


def lingroup_radii(**kwargs):
    import json
    import re

    import pandas as pd

    import linflow.lindb
    import linflow.lingroup
    import linflow.scheme
    import linflow.util

    GTDB_CLUSTER_HEADERS = {
        "Representative genome": "genome",
        "GTDB species": "title",
        "GTDB taxonomy": "taxonomy",
        "ANI circumscription radius": "radius",
    }
    db = linflow.lindb.LINdb(kwargs.get("workspace", "NO WORKSPACE"))
    scheme = linflow.scheme.SchemeSet(db, schemeID=int(kwargs.get("SchemeID", 1)))
    clusters_df = pd.read_csv(kwargs.get("Radii_file", ""), sep="\t")

    tax_type = (
        int(kwargs.get("TaxTypeID", 1))
        if "taxtypeID" not in clusters_df.columns
        else None
    )
    clusters_df["taxType"] = tax_type or clusters_df["taxType"]

    if len(np.intersect1d(clusters_df.columns, [*GTDB_CLUSTER_HEADERS])) == len(
        GTDB_CLUSTER_HEADERS
    ):
        clusters_df["GTDB taxonomy"] = clusters_df[
            ["GTDB taxonomy", "GTDB species"]
        ].agg(";".join, axis=1)
        clusters_df.rename(columns=GTDB_CLUSTER_HEADERS, inplace=True)
        clusters_df.drop(
            columns=np.setdiff1d(
                clusters_df.columns, [*GTDB_CLUSTER_HEADERS.values()]
            ).tolist(),
            inplace=True,
        )
        prepend_size = 7
    else:
        if match := re.search(r"^[A-Z_]+\d", str(clusters_df.at[0, "genome"])):
            prepend_size = match.span()[1] - 1

    # original XX_GCX_00000000000000000.Y
    # remove XX_GCX leaving the assembly middle and version
    clusters_df["assemblyv"] = (
        clusters_df["genome"]
        .apply(lambda x: x[prepend_size:])
        .replace("", np.nan)
        .astype("string")
    )
    # remove XX_GCX and .Y leaving the assembly middle only
    clusters_df["assembly"] = (
        clusters_df["genome"]
        .apply(lambda x: x[prepend_size:-2] if not pd.isna(x) else x)
        .astype("Int64")
    )
    # calculate positions to keep (i.e. new prefix) based on radius percentage
    clusters_df["lin_position"] = clusters_df["radius"].apply(
        lambda x: scheme.tochange_position(float(x) - 1)
    )
    invalid_assemblyIDs = clusters_df[clusters_df["assemblyv"] == ""]["genome"]
    if len(invalid_assemblyIDs):
        logger.warning(
            "Some inputs did not have a valid assemblyID, %s", invalid_assemblyIDs
        )
    clusters_df.dropna(subset=["assemblyv"], inplace=True)

    # skipping placeholder genome 1. Real genomeIDs start from 2
    result = db.execute(
        f"""
                        SELECT g.genomeID, filePath, dlin 
                        FROM Genome g LEFT JOIN DLIN USING (genomeID) 
                        WHERE g.genomeID > 1 AND SchemeSetID = {scheme.id}"""
    )
    genome_df = pd.DataFrame(result, columns=["genomeID", "filePath", "dlin"])
    logger.debug("Loading %d genomes", len(result))
    genome_df["assemblyv"] = (
        genome_df["filePath"]
        .apply(linflow.util.extract_assemblyID)
        .replace("", np.nan)
        .astype("string")
    )
    genome_df["assembly"] = (
        genome_df["assemblyv"]
        .apply(lambda x: x[:-2] if not pd.isna(x) else x)
        .astype("Int64")
    )
    genome_df.dropna(subset=["assemblyv"], inplace=True)

    logger.debug(
        "Matching assemblies for %d genomes with %d radii",
        len(genome_df),
        len(clusters_df),
    )
    merge_df = clusters_df.merge(genome_df, how="left", on="assembly")
    agg_df = merge_df.groupby("assembly").agg(set).reset_index()
    agg_df["prefix"] = np.empty((len(agg_df), 0)).tolist()
    logger.debug("Assemblies matched count is %d", len(agg_df))

    if np.any(agg_df["lin_position"].apply(lambda x: len(x) > 1)):
        logger.warning("There are some lingroups that have different prefix LIN sizes")

    for agg_idx, cluster in agg_df.iterrows():
        # translate dlins to prefixes
        for prefix_idx, dlin in enumerate(cluster["dlin"]):
            # e.g. position 5 means 0-5 == length of 6
            lin_length = list(cluster["lin_position"])[0] + 1
            if not pd.isna(dlin):
                new_slin = linflow.util.dlin2slin(
                    json.loads(dlin), scheme_length=lin_length
                )
                if new_slin not in cluster["prefix"]:
                    agg_df.at[agg_idx, "prefix"] += [new_slin]

    # if two clusters for the same genome
    cluster_duplicates = agg_df[[len(x) > 1 for x in agg_df["genome"]]]
    if not cluster_duplicates.empty:
        logger.warning(
            "There are two or more radii that have the same genome. %d of them",
            len(cluster_duplicates),
        )
    # if genomes with same assembly ID but with different versions are available in the database
    genome_duplicates = agg_df[[len(x) > 1 for x in agg_df["genomeID"]]]
    genome_duplicate_ids = []

    # checking if the two dlins for the same assembly result in the same prefix
    # if not those genomes will be logged and removed
    for agg_idx, cluster in genome_duplicates.iterrows():
        if len(cluster.prefix) > 1:
            logger.warning(
                "Matching genomes were assigned different prefixes %s",
                cluster.to_json(orient="columns"),  # type: ignore
            )
            genome_duplicate_ids += [agg_idx]

    # removing missing (no LIN) and duplicate genomes
    missing_genomes = agg_df[[x == {pd.NA} for x in agg_df["assemblyv_y"]]]
    clean_df = agg_df.drop([*genome_duplicate_ids, *missing_genomes.index])
    set_keys = np.setdiff1d(clean_df.columns, ["assembly", "prefix"])
    clean_df[set_keys] = clean_df[set_keys].applymap(
        lambda x: next(iter(x)), na_action="ignore"
    )  # type: ignore

    # get duplicate prefixes
    prefix_df = clean_df["prefix"].explode()
    prefix_list = np.array(prefix_df.to_list()).flatten()
    uniq_prefix, prefix_count = np.unique(prefix_list, return_counts=True)
    duplicate_prefixes = uniq_prefix[prefix_count > 1]
    # finding prefixes that are shared among multiple lingroups and removing them
    duplicate_prefix_df = clean_df.loc[
        prefix_df[prefix_df.isin(duplicate_prefixes)].index
    ]
    if not duplicate_prefix_df.empty:
        logger.warning(
            "Some calculated prefixes (%d/%d) had overlaps but were not excluded.",
            len(duplicate_prefix_df),
            len(clean_df),
        )
        duplicate_prefix_df.to_json(orient="columns")
    # clean_df.drop(duplicate_prefix_df.index, inplace=True)

    if "taxTypeID" not in clean_df.columns:
        clean_df["taxTypeID"] = kwargs.get("TaxTypeID")

    linflow.lingroup.define_lingroups(db, clean_df, scheme.id, **kwargs)


def lingroup_members(**kwargs):
    """returns the genomeIDs that are part of a LINgroup (which could have multiple prefixes)
    Args:
        SchemeID:int ID of the SchemeSet to search LINs
        One of the below:
        LINgroupID:int ID of the lingroup to get the genome members
        PrefixList:str of the prefixes in the format lin1;lin2;lin3 where lin positions are separated by commas lin1=(1,2,3,0)

    """
    import pandas as pd

    import linflow.lindb
    import linflow.lingroup
    import linflow.scheme
    import linflow.util

    workspace = kwargs.get("workspace", "NO WORKSPACE")
    db = linflow.lindb.LINdb(database_name=workspace)
    schemeID = int(kwargs.get("SchemeID", 1))
    if kwargs.get("LINgroupID"):
        lingroupID = int(kwargs.get("LINgroupID", "0"))
        lingroup_df = db.get_lingroup_prefixes(lingroupID, schemeID)
        if lingroup_df.empty:
            return []
        prefix_dlins = [
            linflow.util.nodes2dlin(db.get_prefix_parents(prefixID, schemeID), True)
            for prefixID in lingroup_df["prefixID"]
        ]
    else:
        prefix_slins = kwargs.get("PrefixList", "").split(";")
        prefix_dlins = [
            linflow.util.slin2dlin(slin, prefix=True, trailing_zeros=True)
            for slin in prefix_slins
        ]

    prefix_dlins, argsort_dlins = linflow.util.sort_dlins(
        prefix_dlins, prefix=True, trailing_zeros=True
    )
    children_df = DataFrame()
    for target_dlin in prefix_dlins:
        max_target_lin_position = max([*target_dlin])
        genome_match_possible = (
            True if target_dlin[max_target_lin_position] > 0 else False
        )
        found_genome_df = db.search_lin(target_dlin, schemeID)
        if found_genome_df.empty:
            continue
        best_match_index = found_genome_df[
            found_genome_df["idx"] == found_genome_df.max()["idx"]
        ].index[0]
        if (
            genome_match_possible
            and found_genome_df.at[best_match_index, "idx"] == max_target_lin_position
        ):
            # if exact match found (should be non zero ending prefix) => add all of its children
            children_df = pd.concat(
                [
                    children_df,
                    db.get_lin_child(
                        found_genome_df.at[best_match_index, "genomeID"], schemeID
                    ),
                ]
            )
        elif not genome_match_possible:
            # if exact match does not exist (a.k.a zero ending prefix) => add all of its children
            target_parent = found_genome_df.at[best_match_index, "genomeID"]
            children_match = db.get_lin_child(target_parent, schemeID)
            acceptable_children = children_match["idx"] > max_target_lin_position
            # list if intermediary parents that are not eligible, excluding the original parent
            exclude_parents = np.setdiff1d(
                children_match[~acceptable_children]["genomeID"], target_parent
            )
            # only add children where their parents are acceptable children
            children_df = pd.concat(
                [
                    children_df,
                    children_match[
                        acceptable_children
                        & ~children_match["parent"].isin(exclude_parents)
                    ],
                ]
            )
        else:
            # if there could be a best match but none was found (should not exist)
            assert found_genome_df.at[best_match_index, "idx"] < max_target_lin_position
            continue

    found_genomes = children_df["genomeID"]
    noun = "genome" if len(children_df) == 1 else "genomes"
    return {
        "data": found_genomes.tolist(),
        "type": "genome-table",
        "title": "LINgroup Members",
        "subtitle": f"{len(found_genomes)} {noun} found",
        "schemeID": schemeID,
    }


def all(**kwargs):
    """runs through the entire pipeline add->summary->tree
    Args:
            All args from add, summary, and tree
    """
    genome(**kwargs)
    df = summary(**kwargs)
    tree(**kwargs)
    return df


def add_attributes(**kwargs):
    import re

    import numpy as np
    import pandas as pd

    att_file = os.path.abspath(kwargs.get("File", ""))
    update_cols = kwargs.get("Update_cols") or []
    date_cols = kwargs.get("Date_cols") or []

    workspace = kwargs.get("workspace", "NO WORKSPACE")

    if not os.path.isfile(att_file):
        logger.error("Attribute file not found '{att_file}'")
        raise FileNotFoundError("Attribute file not found '%s'", att_file)

    sep = "\t" if re.match(r".*\.tsv$", att_file, re.IGNORECASE) else ","
    db = linflow.lindb.LINdb(database_name=workspace)
    file_df = pd.read_csv(att_file, header=0, encoding="unicode_escape", sep=sep)
    if "Unnamed: 0" in file_df.columns:
        file_df.drop(columns=["Unnamed: 0"], inplace=True)

    # removing non-integer header columns
    variable_columns = [int(col) for col in file_df.columns if col != "genomeID"]
    # set to all columns if only [0] is given as input
    if update_cols == [0]:
        update_cols = variable_columns
    if date_cols == [0]:
        date_cols = variable_columns

    try:
        variable_columns = np.array(variable_columns, dtype=np.uint)
        rename_dict = {f"{col}": col for col in variable_columns}
        file_df.rename(columns=rename_dict, inplace=True)

    except BaseException:
        linflow.exceptions.handle_exception(
            TypeError(
                "Some columns in the file were not attributeIDs (non-integer)\n%s",
                variable_columns,
            ),
            logger,
        )

    # checking update columns
    for set_cols in update_cols + date_cols:
        error_cols = np.setdiff1d(set_cols, variable_columns)
        if len(error_cols) > 0:
            logger.error(
                "Columns '%s' did not exist in file '%s'", error_cols, att_file
            )
    # parsing dat columns
    for col in date_cols:
        try:
            file_df[col] = (
                pd.to_datetime(
                    file_df[col],
                    errors="coerce",
                    format="%m/%d/%Y",
                    infer_datetime_format=False,
                )
                .apply(lambda x: (f"{x:%Y-%m-%d}" if not pd.isna(x) else ""))
                .astype(str)
                .replace("", np.nan)
            )
        except BaseException as e:
            file_df.drop(columns=[col], inplace=True)
            linflow.exceptions.handle_exception(e, logger, raise_exception=False)

    # get new set of columns after drops
    variable_columns = [col for col in file_df.columns if col != "genomeID"]
    # make columns into rows
    att_df = pd.melt(
        file_df,
        id_vars=["genomeID"],
        value_vars=variable_columns,
    )
    att_df["variable"] = att_df["variable"].astype(int)
    att_df.dropna(subset=["value"], inplace=True)
    att_df.rename(columns={"variable": "attID", "value": "attValue"}, inplace=True)
    if update_cols:
        db.update_attributes(att_df[att_df["attID"].isin(update_cols)], overwrite=True)
    db.update_attributes(att_df[~att_df["attID"].isin(update_cols)])


def find_lingroup(**kwargs):
    import json

    workspace = kwargs.get("workspace", "NO WORKSPACE")
    keyword = kwargs.get("Keyword", "")
    submitter = kwargs.get("Submitter", "")
    tax_typeID = kwargs.get("TaxTypeID", 0)
    """tax_rank_dict = {
        int(k): v for k, v in json.loads(kwargs.get("TaxRanks", "{}")).items()
    }"""
    input_tax_rank = kwargs.get("TaxRanks", "").strip()
    tax_rank_dict = {}
    
    if input_tax_rank:
        tax_rank_list = input_tax_rank.split("$")
        # first element is rankID second is rankName (taxon)
        tax_rank_dict = {
            int(tax_rank_list[i]): tax_rank_list[i + 1]
            for i in range(0, len(tax_rank_list), 2)
        }

    db = linflow.lindb.LINdb(workspace)
    # we get both prefixIDs and LINgroupIDs
    result_df = db.search_lingroup(tax_rank_dict, keyword, tax_typeID, submitter)
    result = np.unique(result_df["prefixID"]).tolist()
    noun = "prefix" if len(result) == 1 else "prefixes"
    return {
        "data": result,
        "type": "lingroup-table",
        "title": "Matching LINgroups",
        "subtitle": f"{len(result)} {noun} found",
    }
